//
//  GameScene.swift
//  Project17
//
//  Created by Ruben Dias on 17/04/2020.
//  Copyright © 2020 Ruben Dias. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var starfield: SKEmitterNode!
    var player: SKSpriteNode!

    var scoreLabel: SKLabelNode!
    var score = 0 {
        didSet {
            scoreLabel.text = "Score: \(score)"
        }
    }
    
    let possibleEnemies = ["ball", "hammer", "tv"]
    var enemiesCreated = 0 {
        didSet {
            if enemiesCreated % 15 == 0 {
                updateTimer()
            }
        }
    }
    var gameTimer: Timer?
    var initialTimeInterval: TimeInterval = 0.7
    var currentTimeInterval: TimeInterval = 0.7
    
    var gameOver: SKSpriteNode!
    var newGame: SKLabelNode!
    var isGameOver = false
    
    var initialTouchLocation: CGPoint!
    
    override func didMove(to view: SKView) {
        backgroundColor = .black

        starfield = SKEmitterNode(fileNamed: "starfield")!
        starfield.position = CGPoint(x: 1024, y: 384)
        starfield.advanceSimulationTime(10)
        starfield.zPosition = -1
        addChild(starfield)

        player = SKSpriteNode(imageNamed: "player")
        player.position = CGPoint(x: 100, y: 384)
        player.physicsBody = SKPhysicsBody(texture: player.texture!, size: player.size)
        player.physicsBody?.contactTestBitMask = 1
        addChild(player)

        scoreLabel = SKLabelNode(fontNamed: "Chalkduster")
        scoreLabel.position = CGPoint(x: 16, y: 16)
        scoreLabel.horizontalAlignmentMode = .left
        addChild(scoreLabel)
        
        gameOver = SKSpriteNode(imageNamed: "gameOver")
        gameOver.position = CGPoint(x: 512, y: 384)
        gameOver.zPosition = 1
        
        newGame = SKLabelNode(fontNamed: "Marker Felt")
        newGame.text = "Play Again"
        newGame.position = CGPoint(x: 512, y: 300)
        newGame.zPosition = 1
        newGame.horizontalAlignmentMode = .center
        newGame.fontSize = 38

        physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        physicsWorld.contactDelegate = self
        
        gameTimer = Timer.scheduledTimer(timeInterval: currentTimeInterval, target: self, selector: #selector(createEnemy), userInfo: nil, repeats: true)
    }
    
    func updateTimer() {
        gameTimer?.invalidate()
        
        if enemiesCreated == 0 {
            currentTimeInterval = initialTimeInterval
        }
        if currentTimeInterval >= 0.2 {
            currentTimeInterval -= 0.1
        }
        print(currentTimeInterval)
        gameTimer = Timer.scheduledTimer(timeInterval: currentTimeInterval, target: self, selector: #selector(createEnemy), userInfo: nil, repeats: true)
    }
    
    @objc func createEnemy() {
        guard let enemy = possibleEnemies.randomElement() else { return }

        let sprite = SKSpriteNode(imageNamed: enemy)
        sprite.position = CGPoint(x: 1200, y: Int.random(in: 50...736))
        addChild(sprite)

        sprite.physicsBody = SKPhysicsBody(texture: sprite.texture!, size: sprite.size)
        sprite.physicsBody?.categoryBitMask = 1
        sprite.physicsBody?.velocity = CGVector(dx: -500, dy: 0)
        sprite.physicsBody?.angularVelocity = 5
        sprite.physicsBody?.linearDamping = 0
        sprite.physicsBody?.angularDamping = 0
        
        enemiesCreated += 1
    }
    
    override func update(_ currentTime: TimeInterval) {
        for node in children {
            if node.position.x < -300 {
                node.removeFromParent()
            }
        }

        if !isGameOver {
            score += 1
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        let location = touch.location(in: self)
        
        let tappedNodes = nodes(at: location)
        for node in tappedNodes {
            if let labelNode = node as? SKLabelNode, labelNode.text == "Play Again" {
                score = 0
                
                gameOver.removeFromParent()
                newGame.removeFromParent()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
                    guard let player = self?.player else { return }
                    player.position = CGPoint(x: 100, y: 384)
                    self?.addChild(player)
                    self?.enemiesCreated = 0
                    self?.isGameOver = false
                }
                
                continue
            } else {
                initialTouchLocation = touch.location(in: self)
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else { return }
        let location = touch.location(in: self)
        
        if location.y < 100 || location.y > 668 {
            return
        }

        let dy = -(initialTouchLocation.y - location.y)
        let dx = -(initialTouchLocation.x - location.x)
        
        player.position = CGPoint(x: player.position.x + dx, y: player.position.y + dy)
        initialTouchLocation = location
    }
}

extension GameScene: SKPhysicsContactDelegate {
    
    func didBegin(_ contact: SKPhysicsContact) {
        let explosion = SKEmitterNode(fileNamed: "explosion")!
        explosion.position = player.position
        addChild(explosion)

        player.removeFromParent()
        
        gameTimer?.invalidate()

        isGameOver = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { [weak self] in
            guard let self = self else { return }
            self.addChild(self.gameOver)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { [weak self] in
                guard let self = self else { return }
                self.addChild(self.newGame)
            }
        }
    }
}
